const { Post } = require('../models');

class Posts {
    static findAll(req, res) {
        Post.findAll()
            .then((posts) => {
                if (posts) {
                    res.status(200).json({
                        status: 'Success',
                        posts
                    })
                } else {
                    res.status(404).json({
                        status: 'Fail',
                        message: `Data Posts not found`
                    })
                }
            }).catch((err) => {
                res.status(422).json({
                    status: 'Fail',
                    error: [err.message]
                })
            });
    }

    static create(req, res) {
        Post.create(req.body)
            .then((post) => {
                res.status(201).json({
                    status: 'Success',
                    data: { post }
                })
            }).catch((err) => {
                res.status(422).json({
                    status: 'Fail',
                    error: [err.message]
                })
            });
    }

    static update(req, res) {
        let id = req.params.id
        Post.update(req.body, { where: { id } })
            .then((post) => {
                res.status(202).json({
                    status: 'Success',
                    data: { post }
                })
            }).catch((err) => {
                res.status(422).json({
                    status: 'Fail',
                    error: [err.message]
                })
            });
    }

    static delete(req, res) {
        let id = req.params.id
        Post.destroy({ where: { id } })
            .then((post) => {
                res.status(200).json({
                    status: 'Success',
                    data: { post }
                })
            }).catch((err) => {
                res.status(422).json({
                    status: 'Fail',
                    error: [err.message]
                })
            });
    }
}

module.exports = Posts
