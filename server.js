const express = require('express');
const app = express()
const logger = require('morgan');
require('dotenv').config();

const Router = require('./router');

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

if (process.env.NODE_ENV !== 'test') {
    app.use(logger('dev'))
}

app.use('/', Router)

module.exports = app